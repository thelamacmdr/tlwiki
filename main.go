package main

import (
	"fmt"
	"golang.org/x/net/html"
	"net/http"
	"os"
	"regexp"
)

func main() {
	var root string = "https://tlwiki.org"
	htm, _ := http.Get(root + "/index.php?title=Idolmaster_SP:" + os.Args[1])
	doc, err := html.Parse(htm.Body)
	info := make(chan string)
	if err != nil {
		fmt.Println(err)
	}

	go Mcparsey(doc, "a", "Idolmaster_SP:"+os.Args[1]+":[0-9]*", info)
	for i := range info {
		Mcdownload(root + i)
	}
}

func Mcparsey(n *html.Node, element string, match string, info chan string) {
	if n.Type == html.ElementNode && n.Data == element {
		if match == "meh" {
			m := n.FirstChild
			info <- m.Data
		} else {
			for _, val := range n.Attr {
				matched, _ := regexp.MatchString(match, val.Val)
				if matched {
					info <- val.Val
				}
			}
		}
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		Mcparsey(c, element, match, info)
	}

	if n.Type == html.ElementNode && n.Data == "html" {
		close(info)
	}
}

func Mcdownload(url string) {
	re, _ := regexp.Compile("[0-9]+")
	filename := re.FindString(url) + ".txt"
	fmt.Println(filename)
	file, err := os.Create(filename)
	if err != nil {
		fmt.Println(err)
	}

	htm, _ := http.Get(url)
	doc, _ := html.Parse(htm.Body)
	pre := make(chan string, 10)
	go Mcparsey(doc, "pre", "meh", pre)
	for i := range pre {
		file.WriteString(i)
	}
}
